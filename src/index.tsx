import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'provider/context'
import { Main } from 'layouts'

ReactDOM.render(
  <React.StrictMode>
    <Provider>
      <Main />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)
