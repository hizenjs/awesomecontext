import { useProviderValue } from 'hooks'

export type ContextType = ReturnType<typeof useProviderValue>
