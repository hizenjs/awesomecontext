import React from 'react'
import { useContext } from 'provider/context'

export default function useListenMouseMove(): void {
  const { setMoved, setPoint } = useContext()
  const isMounted = React.useRef(false)
  React.useEffect(() => {
    isMounted.current = true
    const listen = (e: MouseEvent) => {
      if (isMounted.current) {
        setPoint((draft: { x: number; y: number }) => {
          draft.x = e.x
          draft.y = e.y
        })
        setMoved(true)
      }
    }
    document.addEventListener('mousemove', listen)
    return () => {
      isMounted.current = false
      document.removeEventListener('mousemove', listen)
    }
  }, [setMoved, setPoint])
  return
}
