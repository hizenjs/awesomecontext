import { useContext } from 'provider/context'

export default function useMoved(): boolean {
  const { moved } = useContext()
  return moved
}
