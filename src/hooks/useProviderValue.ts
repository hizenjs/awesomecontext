import { useMemo, useState } from 'react'
import { useImmer } from 'use-immer'

export default function useProviderValue(): any {
  const [moved, setMoved] = useState(false)
  const [point, setPoint] = useImmer<{
    x: number
    y: number
  }>({ x: 0, y: 0 })
  return useMemo(
    () => ({
      moved,
      setMoved,
      point,
      setPoint,
    }),
    [moved, point, setPoint],
  )
}
