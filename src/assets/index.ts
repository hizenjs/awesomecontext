import flowers from './img/flowers.png'
import original from './brand/original.png'
import blue from './brand/blue.png'

const logo = {
  original,
  blue,
}

const icons = {}

const images = {
  flowers,
}

export default { logo, icons, images }
