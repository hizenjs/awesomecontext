import React, { Props, ReactElement, ReactNode } from 'react'
import { useProviderValue } from 'hooks'
import { ContextType } from 'types'

const Context = React.createContext<ContextType | undefined>(undefined)
Context.displayName = 'MouseContext'

export function useContext(): ContextType {
  const context = React.useContext(Context)
  if (context === undefined) {
    throw new Error(`useContext must be used within a Provider`)
  }
  return context
}

export const Provider = (props: Props<ReactNode>): ReactElement => {
  const value = useProviderValue()
  return <Context.Provider value={value} {...props} />
}
