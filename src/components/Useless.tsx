import { useListenMouseMove } from 'hooks'

export const Useless = (): null => {
  useListenMouseMove()
  return null
}
