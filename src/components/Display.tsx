import React from 'react'
import { useMoved } from '../hooks'
import { useContext } from 'provider'

export const Display = (): React.ReactElement => {
  const { point } = useContext()
  const hasMoved = useMoved()
  return (
    <>
      {hasMoved && (
        <span>
          ({point.x},{point.y})
        </span>
      )}
    </>
  )
}
