import React, { ReactElement } from 'react'
import { Useless, Display } from '../components'
import styled from 'styled-components'

const OldElPaso = styled.div``

const Main = (): ReactElement => (
  <OldElPaso>
    <Useless />
    <Display />
  </OldElPaso>
)

export default Main
